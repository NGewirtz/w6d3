/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

const FollowToggle = __webpack_require__(1);
const UsersSearch = __webpack_require__(3);

$( () => {
  $('.follow-toggle').each(function(i, el){
    new FollowToggle($(el));
  });

  $('.users-search').each(function(i, el){
    new UsersSearch($(el));
  });

});


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

const APIUtil = __webpack_require__(2);

class FollowToggle {
  constructor($el, options) {
    this.$el = $el;
    this.userId = $el.data('user-id') || options.userId;
    this.followState = $el.data('initial-follow-state') || options.followState ;
    this.render();
    $el.on('click', this.handleClick.bind(this));
    this.toggleButton = this.toggleButton.bind(this);
  }

  render(){
    if(this.followState === "unfollowed") {
      this.$el.html("Follow!");
      this.$el.prop('disabled', false);
    }else if(this.followState === 'followed'){
      this.$el.html("Unfollow!");
      this.$el.prop('disabled', false);
    }else if (this.followState === 'following') {
      this.$el.prop('disabled', true);
    }else if (this.followState === 'unfollowing'){
      this.$el.prop('disabled', true);
    }
  }

  handleClick(e) {
    e.preventDefault();
    if (this.followState === 'followed') {
      this.followState = 'unfollowing';
      this.render();
      APIUtil.unfollowUser(this.userId).then(this.toggleButton);
    }else{
      this.followState = 'following';
      this.render();
      APIUtil.followUser(this.userId).then(this.toggleButton);
    }
  }

  toggleButton() {
    this.followState = this.followState === 'unfollowing' ? 'unfollowed' : 'followed';
    this.render();
  }
}


module.exports = FollowToggle;


/***/ }),
/* 2 */
/***/ (function(module, exports) {

const APIUtil = {
  followUser: id => {
    return $.ajax({
      url: `/users/${id}/follow`,
      type: 'POST',
      dataType: 'json',

    });
  },

  unfollowUser: id => {
    return $.ajax({
      url: `/users/${id}/follow`,
      type: 'DELETE',
      dataType: 'json',

    });
  },

  searchUsers: function(queryVal, success){
    return $.ajax({
      url: "/users/search",
      type: 'GET',
      dataType: 'json',
      data: { query: queryVal },
      success: success,
    });
  }
};

module.exports = APIUtil;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

const APIUtil = __webpack_require__(2);
const FollowToggle = __webpack_require__(1);

class UsersSearch {
  constructor($el){
    this.$el = $el;
    this.ul = $el.find("ul");
    this.handleInput = this.handleInput.bind(this);
    $el.find(".search-text").on('input', this.handleInput );
  }

  handleInput(e){
    e.preventDefault();
    this.input = $(e.currentTarget).val();

    APIUtil.searchUsers(this.input, (data) => this.renderResults(data));
  }

  renderResults(data) {
    this.ul.empty();
    data.forEach((el) => {
      let options = {
        userId: el.id,
        followState: el.followed ? 'followed' : 'unfollowed'
      };
      let $li = $('<li>');
      let $button = $('<button class = "follow-toggle"></button>');
      new FollowToggle(el, options);
      $li.html(el.username);
      $li.append($button);
      this.ul.append($li);
    });

  }
}


module.exports = UsersSearch;


/***/ })
/******/ ]);
//# sourceMappingURL=bundle.js.map