const APIUtil = require('./api_util.js');
const FollowToggle = require('./follow_toggle.js');

class UsersSearch {
  constructor($el){
    this.$el = $el;
    this.ul = $el.find("ul");
    this.handleInput = this.handleInput.bind(this);
    $el.find(".search-text").on('input', this.handleInput );
  }

  handleInput(e){
    e.preventDefault();
    this.input = $(e.currentTarget).val();

    APIUtil.searchUsers(this.input, (data) => this.renderResults(data));
  }

  renderResults(data) {
    this.ul.empty();
    data.forEach((el) => {
      let options = {
        userId: el.id,
        followState: el.followed ? 'followed' : 'unfollowed'
      };
      let $li = $('<li>');
      let $button = $('<button class = "follow-toggle"></button>');
      new FollowToggle(el, options);
      $li.html(el.username);
      $li.append($button);
      this.ul.append($li);
    });

  }
}


module.exports = UsersSearch;
