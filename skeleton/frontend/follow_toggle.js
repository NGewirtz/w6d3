const APIUtil = require('./api_util.js');

class FollowToggle {
  constructor($el, options) {
    this.$el = $el;
    this.userId = $el.data('user-id') || options.userId;
    this.followState = $el.data('initial-follow-state') || options.followState ;
    this.render();
    $el.on('click', this.handleClick.bind(this));
    this.toggleButton = this.toggleButton.bind(this);
  }

  render(){
    if(this.followState === "unfollowed") {
      this.$el.html("Follow!");
      this.$el.prop('disabled', false);
    }else if(this.followState === 'followed'){
      this.$el.html("Unfollow!");
      this.$el.prop('disabled', false);
    }else if (this.followState === 'following') {
      this.$el.prop('disabled', true);
    }else if (this.followState === 'unfollowing'){
      this.$el.prop('disabled', true);
    }
  }

  handleClick(e) {
    e.preventDefault();
    if (this.followState === 'followed') {
      this.followState = 'unfollowing';
      this.render();
      APIUtil.unfollowUser(this.userId).then(this.toggleButton);
    }else{
      this.followState = 'following';
      this.render();
      APIUtil.followUser(this.userId).then(this.toggleButton);
    }
  }

  toggleButton() {
    this.followState = this.followState === 'unfollowing' ? 'unfollowed' : 'followed';
    this.render();
  }
}


module.exports = FollowToggle;
